# exercism-solutions

My solutions to the exercises provided at [Exercism](https://exercism.io). Be
sure to try it yourself - it's a very rewarding experience!

# Where is the code?

Switch to a language branch - everything is there.

- [clojure](https://gitlab.com/BrightOne/exercism-solutions/tree/clojure/clojure)
  ![GitLab CI](https://img.shields.io/gitlab/pipeline/BrightOne/exercism-solutions/clojure.svg?style=flat-square)
- [elixir](https://gitlab.com/BrightOne/exercism-solutions/tree/elixir/elixir)
  ![GitLab CI](https://img.shields.io/gitlab/pipeline/BrightOne/exercism-solutions/elixir.svg?style=flat-square)
- [go](https://gitlab.com/BrightOne/exercism-solutions/tree/go/go)
  ![GitLab CI](https://img.shields.io/gitlab/pipeline/BrightOne/exercism-solutions/go.svg?style=flat-square)
- [javascript](https://gitlab.com/BrightOne/exercism-solutions/tree/javascript/javascript)
  ![GitLab CI](https://img.shields.io/gitlab/pipeline/BrightOne/exercism-solutions/javascript.svg?style=flat-square)
- [lua](https://gitlab.com/BrightOne/exercism-solutions/tree/lua/lua)
  ![GitLab CI](https://img.shields.io/gitlab/pipeline/BrightOne/exercism-solutions/lua.svg?style=flat-square)
- [ruby](https://gitlab.com/BrightOne/exercism-solutions/tree/ruby/ruby)
  ![GitLab CI](https://img.shields.io/gitlab/pipeline/BrightOne/exercism-solutions/ruby.svg?style=flat-square)
- [rust](https://gitlab.com/BrightOne/exercism-solutions/tree/rust/rust)
  ![GitLab CI](https://img.shields.io/gitlab/pipeline/BrightOne/exercism-solutions/rust.svg?style=flat-square)

# Contributing

Sorry, but it's a continuous learning experience, so no Pull Requests will be
accepted. But feel free to take a look around, fork it, change it, ask me
questions!

# License

All the solutions are licensed under the MIT license, to my name. All the test
files and per-solution `README.md` files are licensed under the MIT license, to
Exercism (taken from their respective
[repositories](https://github.com/exercism)).
